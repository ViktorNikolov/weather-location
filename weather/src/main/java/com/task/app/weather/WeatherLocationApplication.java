package com.task.app.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeatherLocationApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeatherLocationApplication.class, args);
	}

}
