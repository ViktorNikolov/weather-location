package com.task.app.weather.controller;

import com.task.app.weather.model.WeatherLocation;
import com.task.app.weather.service.WeatherLocationService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping
public class WeatherController {

    private final WeatherLocationService weatherLocationService;

    public WeatherController(WeatherLocationService weatherLocationService) {
        this.weatherLocationService = weatherLocationService;
    }

    @GetMapping("/")
    public String showHomePage() {
        return "index";
    }

    @GetMapping("/name")
    public WeatherLocation getByNameFromApi() {
        return weatherLocationService.getByNameFromWeatherApi();
    }

    @GetMapping("/data")
    public String showAll(@ModelAttribute WeatherLocation weather, Model model) {
        model.addAttribute("weathers", weatherLocationService.getAllWeathersFromDataBase());
        model.addAttribute("weather", new WeatherLocation());

        return "data";
    }

    @PostMapping("/{cityName}")
    public void createInDataBase(WeatherLocation weather, @PathVariable String cityName) {
        String date = LocalDate.now().toString();
        weatherLocationService.createInDataBase(weather, cityName, date);
    }
}