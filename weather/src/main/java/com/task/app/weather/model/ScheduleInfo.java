package com.task.app.weather.model;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDateTime;

@Component
@EnableScheduling
public class ScheduleInfo {
    private final SessionFactory sessionFactory;
    private static final String API_KEY = "e1a3da9115100c719503922681176d6c";


    private ScheduleInfo(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    private WeatherLocation getLocationFromIpApi() {
        WeatherLocation weatherLocation = new WeatherLocation();
        String result;
        try {
            URL url = new URL("https://api.ip2loc.com/6P1HZlDBBYczM1ufEvkR5ZIaXXDxT1EJ/detect?" +
                    "include=city");
            URLConnection conn = url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            result = br.readLine();

            JSONObject jsonObject = new JSONObject(result);
            weatherLocation.setCity(jsonObject.getString("city"));

            br.close();

        } catch (IOException | JSONException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return weatherLocation;
    }

    @Scheduled(fixedDelay = 3600000)
    private WeatherLocation getByNameFromWeatherApi() {
        WeatherLocation weatherLocation = new WeatherLocation();
        String result;
        try {
            URL url = new URL("http://api.openweathermap.org/data/2.5/weather?q="
                    + getLocationFromIpApi().getCity() + "&appid=" + API_KEY + "&units=metric");
            URLConnection conn = url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            result = br.readLine();

            JSONObject jsonObject = new JSONObject(result);

            weatherLocation.setTemp(jsonObject.getJSONObject("main").getString("temp"));
            LocalDateTime date = LocalDateTime.now();
            weatherLocation.setDate(date.toString());
            weatherLocation.setCity(jsonObject.getString("name"));
            br.close();

            try (Session session = sessionFactory.openSession()) {
                session.beginTransaction();
                session.save(weatherLocation);
                session.getTransaction().commit();
            }

        } catch (IOException | JSONException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return weatherLocation;
    }
}
