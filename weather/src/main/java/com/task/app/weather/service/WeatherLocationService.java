package com.task.app.weather.service;

import com.task.app.weather.model.WeatherLocation;

import java.util.List;

public interface WeatherLocationService {

    WeatherLocation getLocationFromIpApi();

    WeatherLocation getByNameFromWeatherApi();

    List<WeatherLocation> getAllWeathersFromDataBase();

    void createInDataBase(WeatherLocation weather, String cityName, String date);

}

