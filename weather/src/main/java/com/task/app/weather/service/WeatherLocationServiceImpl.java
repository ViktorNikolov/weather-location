package com.task.app.weather.service;

import com.task.app.weather.model.WeatherLocation;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.json.*;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class WeatherLocationServiceImpl implements WeatherLocationService {
    private static final String API_KEY = "e1a3da9115100c719503922681176d6c";

    private final SessionFactory sessionFactory;

    public WeatherLocationServiceImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public WeatherLocation getLocationFromIpApi() {
        WeatherLocation weatherLocation = new WeatherLocation();
        String result;
        try {
            URL url = new URL("https://api.ip2loc.com/6P1HZlDBBYczM1ufEvkR5ZIaXXDxT1EJ/detect?" +
                    "include=city");
            URLConnection conn = url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            result = br.readLine();

            JSONObject jsonObject = new JSONObject(result);
            weatherLocation.setCity(jsonObject.getString("city"));

            br.close();

        } catch (IOException | JSONException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return weatherLocation;
    }

    @Override
    public WeatherLocation getByNameFromWeatherApi() {
        WeatherLocation weatherLocation = new WeatherLocation();
        String result;
        try {
            URL url = new URL("http://api.openweathermap.org/data/2.5/weather?q="
                    + getLocationFromIpApi().getCity() + "&appid=" + API_KEY + "&units=metric");
            URLConnection conn = url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            result = br.readLine();

            JSONObject jsonObject = new JSONObject(result);

            weatherLocation.setTemp(jsonObject.getJSONObject("main").getString("temp"));
            LocalDateTime date = LocalDateTime.now();
            weatherLocation.setDate(date.toString());
            weatherLocation.setCity(jsonObject.getString("name"));
            br.close();

            try (Session session = sessionFactory.openSession()) {
                session.beginTransaction();
                session.save(weatherLocation);
                session.getTransaction().commit();
            }

        } catch (IOException | JSONException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return weatherLocation;
    }

    @Override
    public List<WeatherLocation> getAllWeathersFromDataBase() {
        try (Session session = sessionFactory.openSession()) {
            Query<WeatherLocation> query = session.createQuery("from WeatherLocation", WeatherLocation.class);
            return query.list();
        }
    }

    @Override
    public void createInDataBase(WeatherLocation weather, String cityName, String date) {
        weather.setTemp(this.getByNameFromWeatherApi().getTemp());
        weather.setCity(this.getLocationFromIpApi().getCity());
        date = LocalDate.now().toString();
        weather.setDate(date);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(weather);
            session.getTransaction().commit();
        }
    }
}